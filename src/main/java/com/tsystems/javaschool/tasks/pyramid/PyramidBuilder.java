package com.tsystems.javaschool.tasks.pyramid;

import java.util.Comparator;
import java.util.List;

public class PyramidBuilder {
    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */

    public int[][] buildPyramid(List<Integer> inputNumbers) throws CannotBuildPyramidException {
        // TODO : Implement your solution here
        // I use the sum of an arithmetic progression to test the possibility of creating a pyramid
        // S - size of list; a0 = 1; d = 1; S = (2 * a1 + d * (n - 1)) * n / 2 => n^2 + n - 2 * S = 0
        // if sqrt(1 + 4 * S) is integer, then arithmetic progression exists
        int[][] result;
        int length, line, column;
        double n;

        try{
            inputNumbers.sort(Comparator.naturalOrder());
        }catch (Error | Exception ex){
            throw new CannotBuildPyramidException("Impossible to create a pyramid! List has illegal values.");
        }
        length = inputNumbers.size();
        if(Math.sqrt(1 + 4 * 2 * length) % 1 !=0) {
            throw new CannotBuildPyramidException("Impossible to create a pyramid!");
        }
        else{
            n = (-1 + Math.sqrt(1+4*2*length))/2;
            line = (int)n;
            column = (int)(2*n-1);
            result = new int[line][column];
            int sch=0;

            for (int i=0; i<line; i++){
                for (int j=line-1-i; j<column-(line-1-i); j+=2){
                    result[i][j] = inputNumbers.get(sch);
                    sch++;
                }
            }
            return result;
        }
    }
}