package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {
    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
     
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // TODO: Implement the logic here
        int indexOfX=0;
        try {
            if (x.size() > y.size() || y.isEmpty() && !x.isEmpty())
                return false;
            else if (x.isEmpty())
                return true;
            else {
                for (Object aY : y) {
                    if (aY.equals(x.get(indexOfX))) {
                        indexOfX++;
                        if (indexOfX == x.size())
                            return true;
                    }
                }
                return false;
            }
        } catch (NullPointerException | IllegalArgumentException ex){
            throw new IllegalArgumentException();
        }
    }
}