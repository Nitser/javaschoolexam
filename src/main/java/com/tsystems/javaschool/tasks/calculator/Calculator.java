package com.tsystems.javaschool.tasks.calculator;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Stack;

public class Calculator {
    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */

    public String evaluate(String statement) {
        // TODO: Implement the logic here

        String result;
        ArrayList<String> resultArray;
        ArrayList<String> initalArray ;
        //make infiks form
        initalArray = doInfiksForm(statement);
        if(initalArray == null)
            return null;
        //make postfiks form
        resultArray = doPostfiksForm(initalArray);
        if(resultArray == null)
            return null;
        //get result
        result = doEvaluation(resultArray);
        if(result == null)
            return null;
        //result for integer value
        if(Double.parseDouble(result.substring(1,result.length()-1))%1==0){
            return String.valueOf((int)(Double.parseDouble(result.substring(1,result.length()-1))));
        }
        //result for double value
        else {
            //make format for rounding to 4 significant digits
            DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.getDefault());
            otherSymbols.setDecimalSeparator('.');
            String pattern = "#.####";
            DecimalFormat decimalFormat = new DecimalFormat(pattern, otherSymbols);
            decimalFormat.setRoundingMode(RoundingMode.CEILING);
            Number n = Double.parseDouble(result.substring(1,result.length()-1));
            return decimalFormat.format(n.doubleValue()).toString();
        }
    }

    //make infiks form
    private ArrayList<String> doInfiksForm(String statement){
        if( statement == null || statement.equals("") )
            return null;
        else {
            StringBuffer parametr = new StringBuffer();
            ArrayList<String> infiks = new ArrayList<>();

            for (int i = 0; i < statement.length(); i++) {
                if (isNumeral(statement.charAt(i))) {
                    parametr.setLength(parametr.length() + 1);
                    parametr.setCharAt(parametr.length() - 1, statement.charAt(i));
                } else if (isDot(statement.charAt(i))) {
                    parametr.setLength(parametr.length() + 1);
                    parametr.setCharAt(parametr.length() - 1, statement.charAt(i));
                } else if (isBracket(statement.charAt(i))) {
                    if (parametr.length() != 0) {
                        infiks.add(parametr.toString());
                        parametr.delete(0, parametr.length());
                    }
                    infiks.add(String.valueOf(statement.charAt(i)));
                } else if (isOperation(statement.charAt(i))) {
                    if (parametr.length() != 0) {
                        infiks.add(parametr.toString());
                        parametr.delete(0, parametr.length());
                    }
                    infiks.add(String.valueOf(statement.charAt(i)));
                }
                else{
                    return null;
                }
            }
            if (parametr.length() != 0)
                infiks.add(parametr.toString());
            return infiks;
        }
    }

    //make postfiks form with Dijkstra's algorithm
    private ArrayList<String> doPostfiksForm(ArrayList<String> infiksArray){
        //stack for temporary storage of elements
        Stack<String> temporaryStack = new Stack<>();
        //result array
        ArrayList<String> postfiksArray = new ArrayList<>();
        //use "$" to indicate the beginning and end of the string
        infiksArray.add("$");
        temporaryStack.push("$");

        //Dijkstra's algorithm
        for(int i=0; i<infiksArray.size(); i++){
            switch (infiksArray.get(i)) {
                case "$":
                    if (temporaryStack.peek().equals("$"))
                        return postfiksArray;
                    else if (temporaryStack.peek().equals("("))
                        return null;
                    else {
                        postfiksArray.add(temporaryStack.pop());
                        i--;
                    }
                    break;
                case "(":
                    temporaryStack.push(infiksArray.get(i));
                    break;
                case ")":
                    if (temporaryStack.peek().equals("$"))
                        return null;
                    else if (temporaryStack.peek().equals("(")){
                        temporaryStack.pop();
                    }
                    else {
                        postfiksArray.add(temporaryStack.pop());
                        i--;
                    }
                    break;
                case "+":
                    if(temporaryStack.peek().equals("$") || temporaryStack.peek().equals("("))
                        temporaryStack.push(infiksArray.get(i));
                    else {
                        postfiksArray.add(temporaryStack.pop());
                        i--;
                    }
                    break;
                case "-":
                    if(temporaryStack.peek().equals("$") || temporaryStack.peek().equals("("))
                        temporaryStack.push(infiksArray.get(i));
                    else {
                        postfiksArray.add(temporaryStack.pop());
                        i--;
                    }
                    break;
                case "*":
                    if(temporaryStack.peek().equals("*") || temporaryStack.peek().equals("/")) {
                        postfiksArray.add(temporaryStack.pop());
                        i--;
                    }
                    else
                        temporaryStack.push(infiksArray.get(i));
                    break;
                case "/":
                    if(temporaryStack.peek().equals("*") || temporaryStack.peek().equals("/")) {
                        postfiksArray.add(temporaryStack.pop());
                        i--;
                    }
                    else
                        temporaryStack.push(infiksArray.get(i));
                    break;
                default: postfiksArray.add(infiksArray.get(i)); break;
            }
        }
        return postfiksArray;
    }

    //value evaluation
    private String  doEvaluation(ArrayList<String> array){
        try {
        for (int i = 0; i < array.size(); i++) {
            if (isOperation(array.get(i).charAt(0))) {
                switch (array.get(i)) {
                    case "+":
                        array.set(i - 2, String.valueOf(plus(Double.parseDouble(array.get(i - 2)), Double.parseDouble(array.get(i - 1)))));
                        array.remove(i);
                        array.remove(i - 1);
                        i -= 2;
                        break;
                    case "-":
                        array.set(i - 2, String.valueOf(minus(Double.parseDouble(array.get(i - 2)), Double.parseDouble(array.get(i - 1)))));
                        array.remove(i);
                        array.remove(i - 1);
                        i -= 2;
                        break;
                    case "*":
                        array.set(i - 2, String.valueOf(mul(Double.parseDouble(array.get(i - 2)), Double.parseDouble(array.get(i - 1)))));
                        array.remove(i);
                        array.remove(i - 1);
                        i -= 2;
                        break;
                    case "/":
                        if (array.get(i - 1).equals("0.0"))
                            return null;
                        else {
                            array.set(i - 2, String.valueOf(div(Double.parseDouble(array.get(i - 2)), Double.parseDouble(array.get(i - 1)))));
                            array.remove(i);
                            array.remove(i - 1);
                            i -= 2;
                        }
                        break;
                }
            }
        }
        return array.toString();
        }catch (NumberFormatException | ArrayIndexOutOfBoundsException ex){
            return null;
        }
    }

    private double plus(double firstParametr, double secondParametr){
        return firstParametr + secondParametr;
    }

    private double minus(double firstParametr, double secondParametr){
        return firstParametr - secondParametr;
    }

    private double mul(double firstParametr, double secondParametr){
        return firstParametr * secondParametr;
    }

    private double div(double firstParametr, double secondParametr){
        return firstParametr / secondParametr;
    }

    private boolean isOperation(char operator){
        return operator == '+' || operator == '-' || operator == '*' || operator == '/';
    }

    private boolean isDot(char symbol){
        return symbol == '.';
    }

    private boolean isNumeral(char numeral){
        return numeral == '0' || numeral == '1' || numeral == '2' || numeral == '3' || numeral == '4' || numeral == '5' || numeral == '6' || numeral == '7' || numeral == '8' || numeral == '9';
    }

    private boolean isBracket(char bracket){
        return bracket == '(' || bracket == ')';
    }
}